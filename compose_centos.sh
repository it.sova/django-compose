#!/usr/bin/env sh
yum install rustc cargo python3 python3-pip python3-devel openssl-devel libffi-devel
pip3 install --upgrade pip
pip3 install wheel setuptools setuptools_rust